const express = require('express');
const app = express();

let path = require("path");

// Creating public shortcuts
app.use(express.static(path.join(__dirname + "/public")));

function pet(name, age, type, favFood) {
    this.name = name;
    this.age = age;
    this.type = type;
    this.favFood = favFood;
}


app.get('/', (req, res) => {
    res.sendFile("index.html");
});

app.get("/pictures", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/pictures.html"));
});

// Save all pets in an array
let jsonArray = [
    new pet("Zacky", 29, "Middle-eastern", "hairy Chicken in bowl on the floor"),
    new pet("Rolandas",77,"Asian","Ice Cream"
)];

app.get("/data", (req, res)=>{
    if(jsonArray.length === 0){
        res.send("No pets in the list");
    } else {
        res.send(jsonArray);
    }

});

app.get("/info", (req, res) => {
    for(let i=0; i<jsonArray.length; i++){
        res.write("Name: "+ jsonArray[i].name);
        res.write("\nType:" + jsonArray[i].type);
        res.write("\nFavourite food: " + jsonArray[i].favFood);
        res.write("\n\n");
    }
    res.send();

});

app.listen(process.env.PORT || 8080);